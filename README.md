# OWM - Owners - versão 0.1.0
Projeto Owners para iniciar backend de sistemas com arquitetura de micro-serviços<br /><br />

## Tecnologias

| Tecnologia | Versão|
| ------ | ----------- |
|**`Java`**|8.\*|
|**`Apache Maven`**|3.3.9|
|**`JUnit`**|latest|

## Builds

Para gerar as builds locais com o maven:
```bash
 mvn clean install
```
Para gerar as builds versionadas para os ambientes de produção e homologação:
```
  mvn clean install -Pdev
```

## Swagger

[http://{domain}:{port}/base-public/swagger-ui.html]
