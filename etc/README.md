# OWM - Owners - Pré requisitos

Pré requisitos do sistema Owners<br /><br />

## monitor.properties

Este arquivo contém todos as propriedades do sistema para a configuração

`app.name`: Nome da aplicação (opcional)<br /><br />

`server.language`: Lingua usada no sistema para a internacionalização (opcional) (default = pt_BR)<br />
`server.country`: País usada no sistema para a internacionalização (opcional)<br /><br />

`swagger.app.name`: Nome que irá aparecer no swagger<br />
`swagger.app.terms-of-service-url`: URL dos termos do uso (opcional)<br />
`swagger.app.license`: Nome da licença do swagger (opcional)<br />
`swagger.app.license-url`: URL da licença do swagger (opcional)<br />
`swagger.app.version`: Versão da implementação do swagger<br /><br />

`authentication.oauth2.url`=Set do url de autenticação do oauth2<br />
`authentication.oauth2.client`=Set do username do client do oauth2<br />
`authentication.oauth2.secret`=Set do secret que será usado no client do oauth2<br />

## messages*.properties

Este arquivo é referente as mensagens do sistema de forma internacionalizada, para que as mensagens do sistema esteja nos conformes, o desenvolvedor deve:

 * Certificar-se que o default localization do sistema setado pelo monitor.properties é valido e possue um arquivo de messages
 * Certificar-se que todos os arquivos de properties possuem as mesmas propriedades contidas
