# OWM - Owners - domain
Modulo de domain do projeto Owners contendo todas as entidades do banco de dados, DTOs, POJOs e repositórios<br /><br />

## Tecnologias

| Tecnologia | Versão|
| ------ | ----------- |
|**`Java`**|8.\*|
|**`Apache Maven`**|3.3.9|

## Builds

Para gerar as builds locais com o maven:
```bash
 mvn clean install
```
