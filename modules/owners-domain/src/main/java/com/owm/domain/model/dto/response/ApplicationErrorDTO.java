package com.owm.domain.model.dto.response;

import java.io.Serializable;

import com.google.common.base.Objects;

public class ApplicationErrorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String mensagem;

    private String descricao;

    /**
     * Construtor
     * @param mensagem
     */
    public ApplicationErrorDTO(String mensagem) {
        super();
        this.mensagem = mensagem;
    }

    /**
     * Construtor
     * @param mensagem
     * @param descricao
     */
    public ApplicationErrorDTO(String mensagem, String descricao) {
        super();
        this.mensagem = mensagem;
        this.descricao = descricao;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return "ApplicationErrorDTO [mensagem=" + mensagem + ", descricao=" + descricao + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(mensagem, descricao);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof ApplicationErrorDTO) {
            ApplicationErrorDTO that = (ApplicationErrorDTO) object;
            return Objects.equal(this.mensagem, that.mensagem) && Objects.equal(this.descricao, that.descricao);
        }
        return false;
    }

}