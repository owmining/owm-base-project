package com.owm.domain.model.dto.response;

import java.io.Serializable;

import com.google.common.base.Objects;

public class UserDTOResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String username;
	private String email;
	private Boolean activated;
	
	public UserDTOResponse(Long id, String username, String email, Boolean activated) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.activated = activated;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getActivated() {
		return activated;
	}
	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	@Override
	public String toString() {
		return "UserDTOResponse [id=" + id + ", username=" + username + ", email=" + email + ", activated=" + activated
				+ "]";
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof UserDTOResponse) {
			UserDTOResponse that = (UserDTOResponse) object;
			return Objects.equal(this.id, that.id);
		}
		return false;
	}
}
