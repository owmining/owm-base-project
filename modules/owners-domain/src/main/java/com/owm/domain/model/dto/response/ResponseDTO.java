package com.owm.domain.model.dto.response;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import com.google.common.base.Objects;

/**
 * Representa o objeto que deve ser retornado por todos os servicos REST
 * @author MATERA Systems
 *
 */
public class ResponseDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Object data;

    private Set<ApplicationErrorDTO> errors;

    /**
     * Construtor
     */
    public ResponseDTO() {
        super();
    }

    /**
     * Construtor
     * @param data
     */
    public ResponseDTO(Object data) {
        super();
        this.data = data;
    }

    /**
     * Construtor
     * @param errors
     */
    public ResponseDTO(Set<ApplicationErrorDTO> errors) {
        super();
        this.errors = errors;
    }

    /**
     * Construtor
     * @param data
     * @param errors
     */
    public ResponseDTO(Object data, Set<ApplicationErrorDTO> errors) {
        super();
        this.data = data;
        this.errors = errors;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Set<ApplicationErrorDTO> getErrors() {
        if (errors == null) {
            errors = new HashSet<>();
        }
        return errors;
    }

    public void setErrors(Set<ApplicationErrorDTO> errors) {
        this.errors = errors;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(data, errors);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof ResponseDTO) {
            ResponseDTO that = (ResponseDTO) object;
            return Objects.equal(this.data, that.data) && Objects.equal(this.errors, that.errors);
        }
        return false;
    }

    @Override
    public String toString() {
        return "ResponseDTO [data=" + data + ", errors=" + errors + "]";
    }

}