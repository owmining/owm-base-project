package com.owm.controller.api.v1;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.owm.domain.model.dto.response.ResponseDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("/api/v1/basic")
public class BasicController {

	private static final Log LOG = LogFactory.getLog(BasicController.class);

	@PreAuthorize("#oauth2.clientHasRole('ROLE_ADMIN') || #oauth2.clientHasRole('ROLE_API')")
	@ApiOperation(tags = { "Basic" }, value = "Rest Basico", notes = "Responsável por testar o sistema.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 500, message = "Erro inesperado.", response = ResponseDTO.class) })
	@RequestMapping(method = RequestMethod.GET)
	public String index() {
		return "Basic Template!\n";
	}

}
