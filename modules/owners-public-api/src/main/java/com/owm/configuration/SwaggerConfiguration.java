package com.owm.configuration;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.owm.enums.internacionalization.SwaggerEnum;
import com.owm.internacionalization.ResourceMessages;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.collect.Lists.*;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {

	@Value("${swagger.app.name}")
	private String title;

	@Value("${swagger.app.terms-of-service-url}")
	private String termsOfServiceUrl;

	@Value("${swagger.app.license}")
	private String license;

	@Value("${swagger.app.license-url}")
	private String licenseUrl;

	@Value("${swagger.app.version}")
	private String version;

	@Bean
	public Docket v1Api() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("v1").apiInfo(apiInfo())
				.useDefaultResponseMessages(false)
				.globalResponseMessage(org.springframework.web.bind.annotation.RequestMethod.POST, getMessages())
				.securitySchemes(newArrayList(apiKey()))
				.securityContexts(newArrayList(securityContext()))
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.matera.acs.controller.api.v1"))
				.build();
	}
	
//	A cada nova versão deve se criar um novo bean de Docket 
//	@Bean
//	public Docket v2Api() {
//		return new Docket(DocumentationType.SWAGGER_2).groupName("v2").apiInfo(apiInfo())
//				.useDefaultResponseMessages(false)
//				.globalResponseMessage(org.springframework.web.bind.annotation.RequestMethod.POST, getMessages())
//				.securitySchemes(newArrayList(apiKey()))
//				.securityContexts(newArrayList(securityContext()))
//				.select()
//				.apis(RequestHandlerSelectors.basePackage("com.matera.acs.controller.api.v2"))
//				.build();
//	}
	
	private List<ResponseMessage> getMessages() {
		return Arrays.asList(
				new ResponseMessageBuilder().code(200).message("OK").responseModel(new ModelRef("RestResponseDTO"))
				.build(),
		new ResponseMessageBuilder().code(400).message("Bad request")
				.responseModel(new ModelRef("RestResponseDTO")).build(),
		new ResponseMessageBuilder().code(401).message("Unauthorized")
				.responseModel(new ModelRef("RestResponseDTO")).build(),
		new ResponseMessageBuilder().code(403).message("Forbidden")
				.responseModel(new ModelRef("RestResponseDTO")).build(),
		new ResponseMessageBuilder().code(500).message("Internal server error")
				.responseModel(new ModelRef("RestResponseDTO")).build());
	}

	private ApiKey apiKey() {
		return new ApiKey("Authorization", "token", "header");
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/.*"))
				.build();
	}

	List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("trusted", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return newArrayList(new SecurityReference("Authorization", authorizationScopes));
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(title).description(ResourceMessages.getMessage(SwaggerEnum.DESCRIPTION))
				.termsOfServiceUrl(termsOfServiceUrl).license(license).licenseUrl(licenseUrl).version(version).build();
	}
}
