package com.owm.exception.business;

import com.owm.enums.internacionalization.BusinessExceptionEnum;
import com.owm.internacionalization.ResourceMessages;

public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	BusinessException() {
		super(ResourceMessages.getMessage(BusinessExceptionEnum.GENERIC));
	}
}
