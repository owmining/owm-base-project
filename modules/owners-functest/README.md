# OWM - Owners - functest
Modulo de testes de integração do projeto Owners<br /><br />

## Tecnologias

| Tecnologia | Versão|
| ------ | ----------- |
|**`Java`**|8.\*|
|**`Apache Maven`**|3.3.9|

## Builds

Para fazer os testes de integração:
```
  mvn clean install -Pfunctest
```
