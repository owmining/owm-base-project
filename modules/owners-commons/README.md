# OWM - Owners - commons
Modulo de commons do projeto Owners contendo as bibliotecas genéricas do sistema<br /><br />

## Tecnologias

| Tecnologia | Versão|
| ------ | ----------- |
|**`Java`**|8.\*|
|**`Apache Maven`**|3.3.9|
|**`JUnit`**|latest|

## Builds

Para gerar as builds locais com o maven:
```bash
 mvn clean install
```
