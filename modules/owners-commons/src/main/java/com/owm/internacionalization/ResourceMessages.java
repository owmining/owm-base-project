package com.owm.internacionalization;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import com.owm.enums.internacionalization.IMessageEnum;

public class ResourceMessages {

	private static final String BUNDLE_NAME = "messages/messages";

	public static String getMessage(IMessageEnum messageEnum, Object... args) {
		ResourceBundle resourceBundle = null;
		resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
		String message = null;
		if (resourceBundle != null) {
			message = resourceBundle.getString(messageEnum.getProp());
			if (message != null && args.length > 0)
				message = MessageFormat.format(message, args);
		}
		return message;
	}

}
