package com.owm.enums.internacionalization;

public enum SystemExceptionEnum implements IMessageEnum {

	USER_NOT_FOUND("exception.system.user_not_found"), UNAUTHORIZED("exception.system.unauthorized"), USER_INACTIVE(
			"exception.system.user_inactive");

	private String prop;

	private SystemExceptionEnum(String prop) {
		this.prop = prop;
	}

	@Override
	public String getProp() {
		return this.prop;
	}

}
