package com.owm.enums;

public enum Roles {
	
	ROLE_API,
	ROLE_APPLICATION,
	ROLE_ADMIN

}
