package com.owm.enums.internacionalization;

public enum HomeEnum implements IMessageEnum {
	
	TITLE("home.title");

	private String prop;
	
	private HomeEnum(String prop) {
		this.prop = prop;
	}

	@Override
	public String getProp() {
		return this.prop;
	}

}
