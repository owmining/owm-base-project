package com.owm.enums.internacionalization;

public enum BusinessExceptionEnum implements IMessageEnum {

	GENERIC("exception.business.generic");

	private String prop;
	
	private BusinessExceptionEnum(String prop) {
		this.prop = prop;
	}

	@Override
	public String getProp() {
		return this.prop;
	}
}
