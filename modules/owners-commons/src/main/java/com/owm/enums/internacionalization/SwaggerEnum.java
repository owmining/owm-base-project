package com.owm.enums.internacionalization;

public enum SwaggerEnum implements IMessageEnum {
	
	DESCRIPTION("swagger.description")
	;

	private String prop;
	
	private SwaggerEnum(String prop) {
		this.prop = prop;
	}

	@Override
	public String getProp() {
		return this.prop;
	}

}
