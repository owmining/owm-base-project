package com.owm.service;

import com.owm.domain.model.dto.request.UserDTORequest;
import com.owm.domain.model.dto.response.UserDTOResponse;

public interface UserService {

	UserDTOResponse createApplicationUser(UserDTORequest userDTO);
	
}
