package com.owm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.owm.domain.model.dto.request.UserDTORequest;
import com.owm.domain.model.dto.response.UserDTOResponse;
import com.owm.domain.model.entity.User;
import com.owm.domain.repository.UserRepository;
import com.owm.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public UserDTOResponse createApplicationUser(UserDTORequest userDTO) {
		
		// Set new User 
		User user = new User();
		user.setUsername(userDTO.getUsername());
		user.setEmail(userDTO.getEmail());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setActivated(true);
		
		// Save new user
		user = userRepository.save(user);
		
		return new UserDTOResponse(user.getId(), user.getUsername(), user.getEmail(), user.getActivated());
		
	}
}
