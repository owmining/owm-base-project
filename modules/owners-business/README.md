# OWM - Owners - Business
Modulo de serviço do projeto Owners contendo as classes de serviço facade design pattern<br /><br />

## Tecnologias

| Technologia | Versão|
| ------ | ----------- |
|**`Java`**|8.\*|
|**`Apache Maven`**|3.3.9|

## Builds

Para gerar as builds locais com o maven:
```bash
 mvn clean install
```
