package com.owm.controller.api;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.owm.domain.model.dto.request.UserDTORequest;
import com.owm.domain.model.dto.response.ResponseDTO;
import com.owm.domain.model.dto.response.UserDTOResponse;
import com.owm.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	private static final Log LOG = LogFactory.getLog(UserController.class);
	
	@Autowired
	UserService userService;
	
	@PreAuthorize("#oauth2.clientHasRole('ROLE_ADMIN')")
	@RequestMapping(method = RequestMethod.GET)
	public String index() {
		return "User Template!\n";
	}
		
	@PreAuthorize("#oauth2.clientHasRole('ROLE_ADMIN') || #oauth2.clientHasRole('ROLE_APPLICATION')")
	@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> create(@RequestBody UserDTORequest userDTO) {
		UserDTOResponse userResponse = userService.createApplicationUser(userDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseDTO(userResponse));
	}
}
