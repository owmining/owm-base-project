package com.owm.security;

import javax.sql.DataSource;

import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

public class CustomJdbcTokenStore extends JdbcTokenStore {
	
	private static final String CUSTOM_ACCESS_TOKEN_INSERT_STATEMENT = "insert into ATGM_OAUTH_ACCESS_TOKEN (ID_TOKEN, TOKEN, ID_AUTHENTICATION, USERNAME, ID_CLIENT, AUTHENTICATION, REFRESH_TOKEN) values (?, ?, ?, ?, ?, ?, ?)";

	private static final String CUSTOM_ACCESS_TOKEN_SELECT_STATEMENT = "select ID_TOKEN, TOKEN from ATGM_OAUTH_ACCESS_TOKEN where ID_TOKEN = ?";

	private static final String CUSTOM_ACCESS_TOKEN_AUTHENTICATION_SELECT_STATEMENT = "select ID_TOKEN, AUTHENTICATION from ATGM_OAUTH_ACCESS_TOKEN where ID_TOKEN = ?";

	private static final String CUSTOM_ACCESS_TOKEN_FROM_AUTHENTICATION_SELECT_STATEMENT = "select ID_TOKEN, TOKEN from ATGM_OAUTH_ACCESS_TOKEN where ID_AUTHENTICATION = ?";

	private static final String CUSTOM_ACCESS_TOKENS_FROM_USERNAME_AND_CLIENT_SELECT_STATEMENT = "select ID_TOKEN, TOKEN from ATGM_OAUTH_ACCESS_TOKEN where USERNAME = ? and ID_CLIENT = ?";

	private static final String CUSTOM_ACCESS_TOKENS_FROM_CLIENTID_SELECT_STATEMENT = "select ID_TOKEN, TOKEN from ATGM_OAUTH_ACCESS_TOKEN where ID_CLIENT = ?";

	private static final String CUSTOM_ACCESS_TOKEN_DELETE_STATEMENT = "delete from ATGM_OAUTH_ACCESS_TOKEN where ID_TOKEN = ?";

	private static final String CUSTOM_ACCESS_TOKEN_DELETE_FROM_REFRESH_TOKEN_STATEMENT = "delete from ATGM_OAUTH_ACCESS_TOKEN where REFRESH_TOKEN = ?";

	private static final String CUSTOM_REFRESH_TOKEN_INSERT_STATEMENT = "insert into ATGM_OAUTH_REFRESH_TOKEN (ID_TOKEN, TOKEN, AUTHENTICATION) values (?, ?, ?)";

	private static final String CUSTOM_REFRESH_TOKEN_SELECT_STATEMENT = "select ID_TOKEN, TOKEN from ATGM_OAUTH_REFRESH_TOKEN where ID_TOKEN = ?";

	private static final String CUSTOM_REFRESH_TOKEN_AUTHENTICATION_SELECT_STATEMENT = "select ID_TOKEN, AUTHENTICATION from ATGM_OAUTH_REFRESH_TOKEN where ID_TOKEN = ?";

	private static final String CUSTOM_REFRESH_TOKEN_DELETE_STATEMENT = "delete from ATGM_OAUTH_REFRESH_TOKEN where ID_TOKEN = ?";

	public CustomJdbcTokenStore(DataSource dataSource) {
		super(dataSource);
		super.setInsertAccessTokenSql(CUSTOM_ACCESS_TOKEN_INSERT_STATEMENT);
		super.setSelectAccessTokenSql(CUSTOM_ACCESS_TOKEN_SELECT_STATEMENT);
		super.setSelectAccessTokenAuthenticationSql(CUSTOM_ACCESS_TOKEN_AUTHENTICATION_SELECT_STATEMENT);
		super.setSelectAccessTokenFromAuthenticationSql(CUSTOM_ACCESS_TOKEN_FROM_AUTHENTICATION_SELECT_STATEMENT);
		super.setSelectAccessTokensFromUserNameAndClientIdSql(CUSTOM_ACCESS_TOKENS_FROM_USERNAME_AND_CLIENT_SELECT_STATEMENT);
		super.setSelectAccessTokensFromClientIdSql(CUSTOM_ACCESS_TOKENS_FROM_CLIENTID_SELECT_STATEMENT);
		super.setDeleteAccessTokenSql(CUSTOM_ACCESS_TOKEN_DELETE_STATEMENT);
		super.setInsertRefreshTokenSql(CUSTOM_REFRESH_TOKEN_INSERT_STATEMENT);
		super.setSelectRefreshTokenSql(CUSTOM_REFRESH_TOKEN_SELECT_STATEMENT);
		super.setSelectRefreshTokenAuthenticationSql(CUSTOM_REFRESH_TOKEN_AUTHENTICATION_SELECT_STATEMENT);
		super.setDeleteRefreshTokenSql(CUSTOM_REFRESH_TOKEN_DELETE_STATEMENT);
		super.setDeleteAccessTokenFromRefreshTokenSql(CUSTOM_ACCESS_TOKEN_DELETE_FROM_REFRESH_TOKEN_STATEMENT);
	}

}
