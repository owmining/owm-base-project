package com.owm.security;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.owm.domain.model.entity.User;
import com.owm.domain.repository.UserRepository;
import com.owm.enums.internacionalization.SystemExceptionEnum;
import com.owm.exception.UserNotActivatedException;
import com.owm.internacionalization.ResourceMessages;

@Component("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(final String login) {

		String lowercaseLogin = login.toLowerCase();

		User userFromDatabase = userRepository.findByUsernameCaseInsensitive(lowercaseLogin);

		if (userFromDatabase == null) {
			throw new UsernameNotFoundException(ResourceMessages.getMessage(SystemExceptionEnum.USER_NOT_FOUND, lowercaseLogin));
		} else if (!userFromDatabase.getActivated()) {
			throw new UserNotActivatedException(ResourceMessages.getMessage(SystemExceptionEnum.USER_INACTIVE, lowercaseLogin));
		}

		Collection<? extends GrantedAuthority> authorities;
		authorities = Arrays.asList(() -> "ROLE_USER");

		return new org.springframework.security.core.userdetails.User(userFromDatabase.getUsername(),
				userFromDatabase.getPassword(), authorities);

	}

}
