# OWM - Owners - Private Api
Modulo web do projeto Owners contendo os controladores privados que correspondem aos serviços rest do sistema<br /><br />

## Tecnologias

| Tecnologia | Versão|
| ------ | ----------- |
|**`Java`**|8.\*|
|**`Apache Maven`**|3.3.9|
|**`Spring Boot`**|1.5.2|

## Builds

Para gerar as builds locais com o maven:
```bash
 mvn clean install
```
