# OWM - Owners - Resources
Projeto Owners para criar um zip do resources necessários<br /><br />

## Technologies

| Technology | Version|
| ------ | ----------- |
|**`Java`**|8.\*|
|**`Apache Maven`**|3.3.9|

## Builds

To generate zip file
```
  mvn clean install
```
