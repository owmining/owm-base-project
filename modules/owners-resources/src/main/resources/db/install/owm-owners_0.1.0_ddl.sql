--AUDITORIA IMPLANTACAO
CREATE TABLE ATGM_PATCH_AUDIT
( NOME_SUB_SIS		VARCHAR2(15) 	NOT NULL,
  NOME_USR_BD		VARCHAR2(30) 	NOT NULL,
  PATCH				VARCHAR2(10) 	NOT NULL,
  DT_EXEC_INI		DATE			NOT NULL,
  DT_EXEC_FIM		DATE ,
  OBS				VARCHAR2(250)
);

comment on column ATGM_PATCH_AUDIT_BZL.NOME_SUB_SIS IS 'Sigla do sistema';
comment on column ATGM_PATCH_AUDIT_BZL.NOME_USR_BD IS 'Usuário do banco';
comment on column ATGM_PATCH_AUDIT_BZL.PATCH IS 'Versão do sistema';
comment on column ATGM_PATCH_AUDIT_BZL.DT_EXEC_INI IS 'Data de inicio de aplicação de deploy';
comment on column ATGM_PATCH_AUDIT_BZL.DT_EXEC_FIM IS 'Data fim de aplicação de deploy';
comment on column ATGM_PATCH_AUDIT_BZL.OBS IS 'Observação do deploy';

INSERT INTO ATGM_PATCH_AUDIT (NOME_SUB_SIS, NOME_USR_BD, PATCH, DT_EXEC_INI, DT_EXEC_FIM, OBS) VALUES('ATGM', USER, '0.1.0', sysdate, NULL, 'INSTALANDO ATG MONITOR 0.1.0');
COMMIT;
